package io.atomicserver.api.events;

import io.atomicserver.api.interfaces.Client;

/**
 * An abstract class who represent an Event.
 * <P>
 * You can inherit this class to create your own event, or use server event.
 * You can trigger an event with {@link io.atomicserver.api.interfaces.PluginManager#dispatchEvent(Event)}
 * </P>
 * @see io.atomicserver.api.events
 */
public abstract class Event {
    private final Client client;

    /**
     * Initialize event with Client.
     * <p>
     * All event need to be Client related.
     * @param client The client who raised the event
     */
    public Event(Client client) {
        this.client = client;
    }

    /**
     * Return the client who raised the event.
     *
     * @return The client who raised the event
     */
    public Client getClient() {
        return client;
    }
}

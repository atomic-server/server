package io.atomicserver.api.events;

import io.atomicserver.api.interfaces.Client;

/**
 * An event triggered when a user connects to the server.
 */
public class ClientConnectedEvent extends Event {
    /**
     * Create a ClientConnectedEvent.
     *
     * @param client The client who logged in
     */
    public ClientConnectedEvent(Client client) {
        super(client);
    }
}

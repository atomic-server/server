package io.atomicserver.api.events;

import io.atomicserver.api.interfaces.Client;
import io.atomicserver.transport.Packet;

/**
 * An event triggered when a user send {@link Packet} to the server.
 */
public class PacketReceivedEvent extends Event {
    private final Packet packet;

    /**
     * Create a PacketReceivedEvent.
     *
     * @param client The client who send in Plugin Development or the client who receive the packet in Client Development.
     * @param packet The received packet.
     */
    public PacketReceivedEvent(Client client, Packet packet) {
        super(client);
        this.packet = packet;
    }

    /**
     * Return the received packet.
     *
     * @return The received packet.
     */
    public Packet getPacket() {
        return packet;
    }
}

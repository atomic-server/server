package io.atomicserver.api.events;

import io.atomicserver.api.interfaces.Client;

/**
 * An event triggered when a user disconnects from the server.
 */
public class ClientDisconnectedEvent extends Event {
    /**
     * Create a ClientDisconnectedEvent.
     *
     * @param client The client who logged out
     */
    public ClientDisconnectedEvent(Client client) {
        super(client);
    }
}

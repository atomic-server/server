package io.atomicserver.api.events;

import java.lang.annotation.*;

/**
 * An annotation that must be present on all methods for listening to an event in a {@link io.atomicserver.api.interfaces.Listener}.
 */
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface EventHandler {

}

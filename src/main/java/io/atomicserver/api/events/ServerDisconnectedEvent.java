package io.atomicserver.api.events;

import io.atomicserver.api.interfaces.Client;

/**
 * An event triggered when a user disconnects from the server.
 */
public class ServerDisconnectedEvent extends Event {
    /**
     * Create a ClientDisconnectedEvent.
     *
     * @param client The current client instance
     */
    public ServerDisconnectedEvent(Client client) {
        super(client);
    }
}

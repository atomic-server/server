package io.atomicserver.api.interfaces;

import io.atomicserver.transport.ResponsePacket;

import javax.json.JsonValue;

public interface ClientApplication {
    /**
     * Send a request a get the response in a Callback
     *
     * @param name The request name
     * @param args The arguments as JsonValue
     * @param callback The callback
     */
    void sendRequest(String name, JsonValue args, ResponsePacket.Callback callback);


    /**
     * Subscribe to trigger
     * <p>
     * !!! WARNING !!! ONLY USE THIS METHOD ON CLIENT SIDE
     *
     * @param name : The trigger's name
     * @param trigger : The trigger executor
     */
    void subscribeToTrigger(String name, TriggerExecutor trigger);

    /**
     * Register a listener
     * <p>
     * All methods annoted by @EventHandler and with an unique Event parameter will be called when this Event
     * will be sended
     * <p>
     * !!! WARNING !!! ONLY USE THIS METHOD ON CLIENT SIDE
     *
     * @param listener The listener  to register
     */
    void registerListener(Listener listener);

    /**
     * Start a thread to listening incoming packet
     * !!! WARNING !!! ONLY USE THIS METHOD ON CLIENT SIDE
     */
    void startListening();
}

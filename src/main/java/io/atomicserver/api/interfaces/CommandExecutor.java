package io.atomicserver.api.interfaces;

import org.jline.reader.Candidate;
import org.jline.reader.LineReader;
import org.jline.reader.ParsedLine;

import java.util.List;

/**
 * A class that manages a Command
 * <p>
 * To register your command see {@link PluginManager#registerCommand(String, CommandExecutor)}
 */
public interface CommandExecutor {
    /**
     * Execute a command requested sended by server (in command line)
     *
     * @param server the server
     * @param name the command name
     * @param args the command arguments
     */
    void onCommand(Server server, String name, String[] args);

    /**
     * Populates candidates with a list of possible completions for the command line.
     * <p>
     * The list of candidates will be sorted and filtered by the LineReader, so that the list of candidates displayed to the user will
     * usually be smaller than the list given by the completer. Thus it is not necessary for the completer to do any matching
     * based on the current buffer. On the contrary, in order for the typo matcher to work, all possible candidates for the
     * word being completed should be returned.
     * <p>
     * Note that the command name can be retrieve by parsedLine.words().get(0);
     * <p>
     * This fonction is called by the function @code{complete(LineReader, ParsedLine, List<Candidate>)} from a @code{Completer} instance.
     * see jline documentation for more informations https://www.javadoc.io/doc/org.jline/jline/latest/org/jline/reader/Completer.html
     *
     * @param reader The line reader
     * @param parsedLine The parsed command line
     * @param list The List of candidates to populate
     */
    void onComplete(LineReader reader, ParsedLine parsedLine, List<Candidate> list);

    /**
     * Print here the help for the command.
     * <p>
     * This function will be called by the help command
     * @param name the command name
     */
    void usage(String name);
}

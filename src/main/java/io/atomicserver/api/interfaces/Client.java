package io.atomicserver.api.interfaces;

import io.atomicserver.exceptions.PacketReceiveException;
import io.atomicserver.transport.Packet;

import java.net.InetAddress;
import java.util.UUID;

/**
 * A class that manages a Client connection
 */
public interface Client {

    /**
     * Get the UUID of the client
     * <p>
     * the UUID is generated when a client connects to the server, a user's UUID changes with each reconnection.
     *
     * @return The client UUID
     */
    UUID getUUID();

    /**
     * Listen for a Packet from the server.
     * <p>
     * This function block the current Thread
     *
     * @return The received Packet, a subclass of Packet
     * @throws PacketReceiveException if an error occurs (like wrong packet type or length)
     */
    Packet receivePacket() throws PacketReceiveException;

    /**
     * Send a packet to the server
     *
     * @param packet the Packet to send
     */
    void sendPacket(Packet packet);

    /**
     * Get the InetAddress of the client
     *
     * @return the InetAddress of the client
     */
    InetAddress getInetAddress();
}

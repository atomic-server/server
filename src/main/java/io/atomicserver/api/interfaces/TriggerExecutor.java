package io.atomicserver.api.interfaces;

import io.atomicserver.transport.ResponsePacket;

import javax.json.JsonValue;

public interface TriggerExecutor {

    /**
     * Execute a trigger sended by server
     *
     * @param client the client instance
     * @param name the name of the request
     * @param args the args of the request in json
     */
    void onTrigger(Client client, String name, JsonValue args);
}

package io.atomicserver.api.interfaces;

import io.atomicserver.transport.RequestPacket;
import io.atomicserver.transport.ResponsePacket;

/**
 * An interface that's manage a command sended by a client
 */
public interface RequestExecutor {
    /**
     * Execute a request sended by a client
     *
     * @param server the server
     * @param sender the client who sended the request
     * @param packet the request packet
     *
     * @return The response packet (can be null)
     */
    ResponsePacket onRequest(Server server, Client sender, RequestPacket packet);
}

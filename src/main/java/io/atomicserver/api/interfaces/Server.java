package io.atomicserver.api.interfaces;

import io.atomicserver.transport.Packet;

import javax.json.JsonObject;
import javax.json.JsonValue;
import java.util.List;
import java.util.logging.Logger;

public interface Server extends Loggable {

    /**
     * Get the server Logger
     *
     * @return The server Logger
     */
    Logger getLogger();

    /**
     * Get all connected clients
     *
     * @return A list of all connected Client
     */
    List<Client> getClients();

    /**
     * Send packet to all connected clients
     *
     * @param packet The packet to send
     */
    void broadcastPacket(Packet packet);

    /**
     * Dispatch a trigger
     * <p>
     * Send a trigger to all clients who registered for it
     *
     * @param name The trigger name
     * @param args The trigger arguments
     */
    void dispatchTrigger(String name, JsonValue args);

    /**
     * Return the PluginManager
     *
     * @return The PluginManager
     */
    PluginManager getPluginManager();

    /**
     * Return the config of the server from file config.json.
     * <p>
     * If file doesn't exists in directory but exists in jar, it will be write on directory.
     * The result is cached on the first call. Call {@link Server#reloadConfig} to reload cache.
     *
     * @return The config as JsonObject
     */
    JsonObject getConfig();

    /**
     * Reload (or load) the config of the server from file config.json.
     * <p>
     * If file doesn't exists in directory but exists in jar, it will be write on directory.
     *
     * @return The config as JsonObject
     */
    JsonObject reloadConfig();
}

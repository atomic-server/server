package io.atomicserver.api.interfaces;

import javax.json.JsonObject;
import java.io.File;

public interface Plugin extends Loggable {

    /**
     * Get the plugin name
     * @return the plugin name
     */
    String getName();

    /**
     * Get the data folder where you can save data
     * <p>
     * The folder is located in plugins/YourPluginName
     * <p>
     * Note that the folder will be created if it's not exists.
     *
     * @return the data folder
     */
    File getDataFolder();

    /**
     * Return the server instance
     *
     * @return the server instance
     */
    Server getServer();

    /**
     * This method is called by the server when the plugin is loaded.
     * <p>
     * When this method is called you have access to all others methods (getServer, getName...) and
     * all plugins that noted as dependencies are loaded too.
     * <p>
     * Note that you can consider this method as your plugin construcor, indeed,
     * the others methods provided by this class may return null in constructor.
     */
    void onLoad();

    /**
     * This method is called by the server when the plugin is going to be unload
     * <p>
     * Note that all plugins that noted as dependencies are still loaded.
     */
    void onUnload();

    /**
     * Return the config of the plugin from file config.json in this plugin data folder.
     * <p>
     * If file doesn't exists in directory but exists in plugin jar, it will be write on directory.
     * The result is cached on the first call. Call {@link Plugin#reloadConfig} to reload cache.
     *
     * @return The config as JsonObject
     */
    JsonObject getConfig();

    /**
     * Return the config of the plugin from file config.json in this plugin data folder.
     * <p>
     * If file doesn't exists in directory but exists in plugin jar, it will be write on directory.
     *
     * @return The config as JsonObject
     */
    JsonObject reloadConfig();


    /**
     * Print here the help for the plugin
     * <p>
     * This function will be called by the help command
     */
    void help();
}

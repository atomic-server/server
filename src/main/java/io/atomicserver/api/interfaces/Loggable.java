package io.atomicserver.api.interfaces;

import java.util.logging.Level;

/**
 * A simple interface to provide shortcut for logging
 *
 * Note that Plugin and Server implements this interface.
 * For the Plugin case the message is prefixed by "[PluginName]"
 */
public interface Loggable {
    /**
     * Log an INFO message
     * <p>
     * If the logger is currently enabled for the INFO message level then the given message is forwarded to all the registered output Handler objects.
     * @param msg The string message (or a key in the message catalog)
     * @see Loggable#log(Level, String)
     */
    void info(String msg);

    /**
     * Log an WARNING message
     * <p>
     * If the logger is currently enabled for the WARNING message level then the given message is forwarded to all the registered output Handler objects.
     * @param msg The string message (or a key in the message catalog)
     * @see Loggable#log(Level, String)
     */
    void warning(String msg);

    /**
     * Log an SEVERE message
     * <p>
     * If the logger is currently enabled for the SEVERE message level then the given message is forwarded to all the registered output Handler objects.
     * @param msg The string message (or a key in the message catalog)
     * @see Loggable#log(Level, String)
     */
    void severe(String msg);

    /**
     * Log a message, with no arguments.
     * <p>
     * If the logger is currently enabled for the given message level then the given message is forwarded to all the registered output Handler objects.
     * @param level One of the message level identifiers, e.g., SEVERE
     * @param msg The string message (or a key in the message catalog)
     */
    void log(Level level, String msg);

    /**
     * Log a message, with associated Throwable information.
     * <p>
     * If the logger is currently enabled for the given message level then the given arguments are stored in a LogRecord which is forwarded to all registered output handlers.
     * Note that the thrown argument is stored in the LogRecord thrown property, rather than the LogRecord parameters property. Thus is it processed specially by output Formatters and is not treated as a formatting parameter to the LogRecord message property.
     * @param level One of the message level identifiers, e.g., SEVERE
     * @param msg The string message (or a key in the message catalog)
     * @param throwable Throwable associated with log message.
     */
    void log(Level level, String msg, Throwable throwable);
}

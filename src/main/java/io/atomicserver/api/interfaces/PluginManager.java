package io.atomicserver.api.interfaces;

import io.atomicserver.api.events.Event;
import io.atomicserver.plugins.JsonPluginInfos;
import io.atomicserver.transport.RequestPacket;

import javax.json.JsonValue;
import java.io.File;

public interface PluginManager {

    /**
     * This method return the plugin directory
     *
     * @return The plugin directory
     */
    File getPluginsDirectory();

    /**
     * Get a plugin by his name
     *
     * @param name the name or desired plugin
     * @return the plugin that correspond to the given name, or null if not loaded
     */
    Plugin getPlugin(String name);

    /**
     * Get all loaded plugins
     *
     * @return An array of all loaded plugins
     */
    Plugin[] getPlugins();

    /**
     * Load all plugins in the plugin directory
     *
     * @return An array of all loaded plugin
     */
    Plugin[] loadPlugins();

    /**
     * Load a plugin with his description
     *
     * @param pluginInfos the plugin description
     * @return the loaded plugin or null if an error occurs
     */
    Plugin loadPlugin(JsonPluginInfos pluginInfos);

    /**
     * Unload all plugins
     */
    void unloadPlugins();

    /**
     * Unload a plugin
     *
     * @param plugin The plugin to unload
     */
    void unloadPlugin(Plugin plugin);

    /**
     * Reload a plugin
     * @param plugin The plugin to reload
     */
    void reloadPlugin(Plugin plugin);

    /**
     * Register a listener
     * <p>
     * All methods annoted by @EventHandler and with an unique Event parameter will be called when this Event
     * will be sended
     *
     * @param listener The listener  to register
     */
    void registerListener(Listener listener);

    /**
     * Send an Event to all registered listener
     * <p>
     * All listeners' methods who handle this event will be called
     *
     * @param event The event to send
     */
    void dispatchEvent(Event event);

    /**
     * Register a command.
     * <p>
     * This command will be added to others server terminal commands
     * @param name The command name
     * @param cmd The CommandExecutor that will be called when the command is asked to be executed
     */
    void registerCommand(String name, CommandExecutor cmd);

    /**
     * Dispatch a command
     * <p>
     * Call all the registered commands that matches with the given name
     *
     * @param name The command name to execute
     * @param args The command arguments
     */
    void dispatchCommand(String name, String[] args);

    /**
     * Register a request
     * <p>
     * The RequestExecutor will be called when a client send a request with a {@link io.atomicserver.transport.RequestPacket}
     *
     * @param name The request name
     * @param cmd The RequestExecutor that will be called when this request is received
     */
    void registerRequest(String name, RequestExecutor cmd);

    /**
     * Dispatch a request
     * <p>
     * Call all the registered requests that matches with the given name
     *
     * @param client The client who send the request
     * @param packet The request packet
     */
    void dispatchRequest(Client client, RequestPacket packet);
}

package io.atomicserver.api;

import io.atomicserver.Utils.JsonTools;
import io.atomicserver.api.interfaces.Plugin;
import io.atomicserver.api.interfaces.Server;
import io.atomicserver.plugins.JsonPluginInfos;
import io.atomicserver.plugins.PluginClassLoader;

import javax.json.JsonObject;
import java.io.*;
import java.util.logging.Level;

/**
 * A base class for implementing a Plugin.
 */
public abstract class AtomicPlugin implements Plugin {

    private Server server;
    private JsonPluginInfos pluginInfos;

    private JsonObject pluginConfig = null;

    /**
     * Base constructor for AtomicPlugin.
     * <p>
     * Please don't subclass this constructor (or just for initialize final attributes), use onLoad to do stuff instead.
     */
    public AtomicPlugin() {
        final ClassLoader classLoader = this.getClass().getClassLoader();
        if (!(classLoader instanceof PluginClassLoader)) {
            throw new IllegalStateException("AtomicPlugin requires " + PluginClassLoader.class.getName());
        }
        ((PluginClassLoader) classLoader).initialize(this);
    }

    /**
     * Init the plugin. This method is called by {@link PluginClassLoader}. DON'T CALL IT YOURSELF.
     *
     * @param server The server.
     * @param pluginInfos The plugin infos.
     */
    public void init(Server server, JsonPluginInfos pluginInfos) {
        this.server = server;
        this.pluginInfos = pluginInfos;
    }

    @Override
    public Server getServer() {
        return server;
    }

    @Override
    public String getName() {
        return pluginInfos.name;
    }

    /**
     * Get this plugin infos.
     * @return the JsonPluginInfos
     */
    public JsonPluginInfos getPluginInfos() {
        return pluginInfos;
    }

    @Override
    public File getDataFolder() {
        File folder = new File(server.getPluginManager().getPluginsDirectory(), getName());
        if (!folder.exists())
            folder.mkdirs();
        return folder;
    }

    @Override
    public JsonObject getConfig() {
        if (pluginConfig == null)
            return reloadConfig();
        else
            return pluginConfig;
    }

    @Override
    public JsonObject reloadConfig() {
        InputStream in = null;
        File configFile = new File(getDataFolder(), "config.json");

        // If file exists in directory, we use it
        try {
            if (configFile.exists())
                in = new FileInputStream(configFile);
        } catch (FileNotFoundException e) {
            log(Level.WARNING, "Error reading config.json", e);
        }

        // Else, we try to use the file from jar, and write it in dir
        if (in == null) {
            in = getClass().getResourceAsStream("/config.json");

            try {
                configFile.createNewFile();
                OutputStream out = new FileOutputStream(configFile);
                out.write(in.readAllBytes());
                out.flush();
            } catch (IOException e) {
                log(Level.WARNING, "Error writing config.json", e);
            }
        }

        if (in != null)
            pluginConfig = JsonTools.createReader(in).readObject();

        return pluginConfig;
    }

    @Override public void info(String msg) { this.log(Level.INFO, msg); }
    @Override public void warning(String msg) { this.log(Level.WARNING, msg); }
    @Override public void severe(String msg) { this.log(Level.SEVERE, msg); }
    @Override public void log(Level level, String msg) { this.log(level, msg, null); }

    @Override
    public void log(Level level, String msg, Throwable throwable) {
        getServer().log(level, "[" + getName() + "] " + msg, throwable);
    }
}

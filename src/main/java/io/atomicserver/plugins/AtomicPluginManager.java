package io.atomicserver.plugins;

import io.atomicserver.api.AtomicPlugin;
import io.atomicserver.api.events.Event;
import io.atomicserver.api.events.EventHandler;
import io.atomicserver.api.interfaces.*;
import io.atomicserver.commands.PluginManagmentCommands;
import io.atomicserver.commands.PluginsCommand;
import io.atomicserver.exceptions.InvalidPluginException;
import io.atomicserver.transport.RequestPacket;
import io.atomicserver.transport.ResponsePacket;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.util.*;
import java.util.logging.Level;

public class AtomicPluginManager implements PluginManager {
    private final Server server;

    private Map<String, Plugin> plugins;

    private Map<String, List<CommandExecutor>> commands;
    private Map<String, List<RequestExecutor>> requests;
    private List<Listener> listeners;

    private JsonPluginInfos[] validPluginsInfosFromDirectoryCache;

    public AtomicPluginManager(Server server) {
        this.server = server;
        plugins = new HashMap<>();
        commands = new HashMap<>();
        requests = new HashMap<>();
        listeners = new LinkedList<>();

        PluginManagmentCommands pluginManagmentCommands = new PluginManagmentCommands(this);
        this.registerCommand("load", pluginManagmentCommands);
        this.registerCommand("reload", pluginManagmentCommands);
        this.registerCommand("unload", pluginManagmentCommands);
        this.registerCommand("plugins", new PluginsCommand(this));
    }

    @Override
    public File getPluginsDirectory() {
        File directory = new File("plugins");
        if (!directory.exists())
            directory.mkdirs();
        return directory;
    }

    /////////////////////////////////////////////////////////
    ///////////////////// LOAD PLUGINS //////////////////////
    /////////////////////////////////////////////////////////

    @Override
    public Plugin[] loadPlugins() {
        JsonPluginInfos[] infos = getValidPluginsInfosFromDirectory(true);

        for (JsonPluginInfos info : infos) {
            loadPlugin(info, null, false);
        }

        return getPlugins();
    }

    private boolean loadDepends(String forPlugin, String[] depends, boolean stopOnError, String asDependencyOf) {
        boolean res;
        Plugin plugin;

        JsonPluginInfos[] infos = getValidPluginsInfosFromDirectory(false);

        for (String depend : depends) {
            res = true;
            if (!depend.equals(asDependencyOf)) { // Recursion protection
                if (getPlugin(depend) == null) {
                    res = false;
                    for (JsonPluginInfos info : infos) {
                        if (info.name.equals(depend)) {
                            plugin = loadPlugin(info, forPlugin, false);
                            res = plugin != null;
                        }
                    }
                }
            }
            else {
                server.log(Level.SEVERE, "Dependency recursion error : `" + depend + "` require `" + asDependencyOf + "` who requiere `" + depend + "`");
                res = false;
            }

            if (!res) {
                server.log(stopOnError ? Level.SEVERE : Level.WARNING, "No dependency found with name `" + depend + "`");
                if (stopOnError) {
                    server.severe("Can't load plugin `" + forPlugin + "`...");
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public Plugin loadPlugin(JsonPluginInfos pluginInfos) {
        return loadPlugin(pluginInfos, null, true);
    }

    public Plugin loadPlugin(JsonPluginInfos pluginInfos, String asDependencyOf, boolean refreshValidPluginInfos) {

        if (asDependencyOf != null && plugins.containsKey(pluginInfos.name))
            return plugins.get(pluginInfos.name);

        if (refreshValidPluginInfos)
            getValidPluginsInfosFromDirectory(true);

        if (plugins.containsKey(pluginInfos.name)) {
            server.warning("A plugin named `" + pluginInfos.name + "` is already loaded");
            return null;
        }

        if (loadDepends(pluginInfos.name, pluginInfos.depends, true, asDependencyOf)) {
            loadDepends(pluginInfos.name, pluginInfos.softdepends, false, asDependencyOf);

            return loadSinglePlugin(pluginInfos);
        }

        return null;
    }

    private Plugin loadSinglePlugin(JsonPluginInfos pluginInfos) {
        server.info("Loading plugin `" + pluginInfos.name + "` from " + pluginInfos.file.getName());
        try {
            PluginClassLoader loader = new PluginClassLoader(server, this, getClass().getClassLoader(), pluginInfos);

            Plugin plugin = loader.plugin;
            plugins.put(plugin.getName(), plugin);
            plugin.onLoad();

            server.info("Plugin `" + plugin.getName() + "` loaded !");
            return plugin;
        } catch (InvalidPluginException e) {
            server.log(Level.WARNING, "Invalid plugin", e);
        } catch (MalformedURLException e) {
            server.log(Level.WARNING, "PluginClassLoader Error", e);
        }
        return null;
    }

    /////////////////////////////////////////////////////////
    //////////////////// UNLOAD PLUGINS /////////////////////
    /////////////////////////////////////////////////////////

    @Override
    public void unloadPlugins() {
        while (plugins.size() > 0) {
            this.unloadPlugin(plugins.values().iterator().next());
        }
    }

    @Override
    public void unloadPlugin(Plugin plugin) {
        List<Plugin> others = new ArrayList<>(plugins.values());
        for (Plugin other : others) {
            if (other != plugin) {
                if (Arrays.asList(((AtomicPlugin)other).getPluginInfos().depends).contains(plugin.getName())) {
                    unloadPlugin(other);
                }
            }
        }

        unloadSinglePlugin(plugin);
    }

    private void unloadSinglePlugin(Plugin plugin) {
        unregisterPluginCommands(plugin);
        unregisterPluginRequest(plugin);
        unregisterPluginListeners(plugin);
        plugins.remove(plugin.getName());
        server.info("Plugin `" + plugin.getName() + "` unloaded !");
    }

    /////////////////////////////////////////////////////////
    //////////////////// RELOAD PLUGINS /////////////////////
    /////////////////////////////////////////////////////////

    @Override
    public void reloadPlugin(Plugin plugin) {
        JsonPluginInfos[] infos = getValidPluginsInfosFromDirectory(true);
        JsonPluginInfos info = null;

        for (JsonPluginInfos anInfo : infos) {
            if (anInfo.name.equals(plugin.getName())) {
                info = anInfo;
                break;
            }
        }
        if (info == null) {
            unloadPlugin(plugin);
            server.severe("Can't find plugin named " + plugin.getName());
        }
        else {
            unloadSinglePlugin(plugin);
            loadPlugin(info);
        }
    }

    /////////////////////////////////////////////////////////
    /////////////////////// REGISTER ////////////////////////
    /////////////////////////////////////////////////////////

    @Override
    public void registerListener(Listener listener) {
        if (!listeners.contains(listener))
            listeners.add(listener);
    }

    @Override
    public void registerCommand(String name, CommandExecutor cmd) {
        List<CommandExecutor> ls = commands.getOrDefault(name, new LinkedList<CommandExecutor>());
        ls.add(cmd);
        commands.put(name, ls);
    }

    @Override
    public void registerRequest(String name, RequestExecutor request) {
        List<RequestExecutor> ls = requests.getOrDefault(name, new LinkedList<RequestExecutor>());
        ls.add(request);
        requests.put(name, ls);
    }

    /////////////////////////////////////////////////////////
    ////////////////////// DISPATCHERS //////////////////////
    /////////////////////////////////////////////////////////

    @Override
    public void dispatchEvent(Event event) {
        for (Listener listener : listeners) {
            Method[] methods = listener.getClass().getDeclaredMethods();
            for (Method method : methods) {

                if (method.isAnnotationPresent(EventHandler.class) && method.getParameterCount() == 1 && method.getParameterTypes()[0].equals(event.getClass())) {
                    // EventHandler present, only one parameter and same type as this event
                    new Thread(() -> {
                        try {
                            method.invoke(listener, event);
                        } catch (IllegalAccessException | InvocationTargetException e) {
                            e.printStackTrace();
                        }
                    }).start();
                }
            }
        }
    }

    @Override
    public void dispatchCommand(String name, String[] args) {
        if (commands.containsKey(name)) {
            List<CommandExecutor> ls = commands.get(name);
            for (CommandExecutor cmd : ls) {
                cmd.onCommand(server, name, args);
            }
        }
        else {
            System.out.println("Unknow command " + name);
        }
    }

    @Override
    public void dispatchRequest(Client client, RequestPacket packet) {
        if (requests.containsKey(packet.getName())) {
            List<RequestExecutor> ls = requests.get(packet.getName());
            for (RequestExecutor request : ls) {
                ResponsePacket response = request.onRequest(server, client, packet);
                if (response != null)
                    client.sendPacket(response);
            }
        }
        else {
            System.err.println("No request found for name " + packet.getName());
        }
    }

    /////////////////////////////////////////////////////////
    //////////////////////// GETTERS ////////////////////////
    /////////////////////////////////////////////////////////
    @Override
    public Plugin getPlugin(String name) {
        return plugins.getOrDefault(name, null);
    }

    @Override
    public Plugin[] getPlugins() {
        return plugins.values().toArray(new Plugin[0]);
    }

    /////////////////////////////////////////////////////////
    ///////////////////////// TOOLS /////////////////////////
    /////////////////////////////////////////////////////////

    public Set<String> getAvailableCommandsNames() {
        return commands.keySet();
    }

    public List<CommandExecutor> getCommandExecutors(String name) {
        return commands.getOrDefault(name, new LinkedList<>());
    }

    private void unregisterPluginCommands(Plugin plugin) {
        Iterator<String> it = requests.keySet().iterator();
        while (it.hasNext()) {
            List<CommandExecutor> ls = commands.get(it.next());
            for (int i = 0; ls != null && i < ls.size(); i++) {
                if (isObjectOfPlugin(plugin, ls.get(i))) {
                    ls.remove(i--);
                    if (ls.size() <= 0)
                        it.remove();
                }
            }
        }
    }

    private void unregisterPluginRequest(Plugin plugin) {
        Iterator<String> it = requests.keySet().iterator();
        while (it.hasNext()) {
            List<RequestExecutor> ls = requests.get(it.next());
            for (int i = 0; ls != null && i < ls.size(); i++) {
                if (isObjectOfPlugin(plugin, ls.get(i))) {
                    ls.remove(i--);
                    if (ls.size() <= 0)
                        it.remove();
                }
            }
        }
    }

    private void unregisterPluginListeners(Plugin plugin) {
        for (int i = 0; i < listeners.size(); i++) {
            if (isObjectOfPlugin(plugin, listeners.get(i))) {
                listeners.remove(i--);
            }
        }
    }

    public boolean isObjectOfPlugin(Plugin plugin, Object object) {
        ClassLoader loader = object.getClass().getClassLoader();

        if (loader instanceof PluginClassLoader) {
            PluginClassLoader pcl = (PluginClassLoader)loader;
            return (pcl.plugin.equals(plugin));
        }
        return false;
    }

    public Plugin getPluginForObject(Object object) {
        ClassLoader loader = object.getClass().getClassLoader();

        if (loader instanceof PluginClassLoader) {
            PluginClassLoader pcl = (PluginClassLoader)loader;
            return (pcl.plugin);
        }
        return null;
    }

    public JsonPluginInfos[] getValidPluginsInfosFromDirectory(boolean forceRefresh) {
        if (!forceRefresh && validPluginsInfosFromDirectoryCache != null)
            return validPluginsInfosFromDirectoryCache;

        File directory = getPluginsDirectory();
        File[] pluginsJars = directory.listFiles((File pathname) -> pathname.isFile() && pathname.getName().endsWith(".jar"));
        ArrayList<JsonPluginInfos> result = new ArrayList<>();

        for (File file : pluginsJars) {
            try {
                result.add(new JsonPluginInfos(file));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        validPluginsInfosFromDirectoryCache = result.toArray(new JsonPluginInfos[0]);
        return validPluginsInfosFromDirectoryCache;
    }

    public Class<?> getClassByName(String name) {
        for (Plugin plugin : plugins.values()) {
            PluginClassLoader pcl = (PluginClassLoader)plugin.getClass().getClassLoader();
            try {
                return pcl.findClass(name, false);
            } catch (ClassNotFoundException e) {}
        }
        return null;
    }
}

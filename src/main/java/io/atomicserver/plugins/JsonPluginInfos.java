package io.atomicserver.plugins;

import io.atomicserver.Utils.JsonTools;

import javax.json.Json;
import javax.json.JsonObject;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;

public class JsonPluginInfos {
    final public String name;
    final public String mainClassName;
    final public File file;
    final public String[] depends;
    final public String[] softdepends;

    public JsonPluginInfos(File file) throws IOException {

        this.file = file;
        JarFile jarFile = new JarFile(file);

        ZipEntry plugin_json = jarFile.getEntry("plugin.json");
        if (plugin_json == null)
            throw new IOException("Unable to read plugin.json from " + file.getName());
        InputStream pluginStream = jarFile.getInputStream(plugin_json);
        JsonObject json = JsonTools.createReader(pluginStream).readObject();

        this.name = json.getString("name", null);
        this.mainClassName = json.getString("main", null);
        this.depends = JsonTools.getArray(json, "depends", new String[0]);
        this.softdepends = JsonTools.getArray(json, "softdepends", new String[0]);
    }
}

package io.atomicserver.plugins;

import io.atomicserver.api.AtomicPlugin;
import io.atomicserver.api.interfaces.Server;
import io.atomicserver.exceptions.InvalidPluginException;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.Map;

public class PluginClassLoader extends URLClassLoader {

    private final Server server;
    private final AtomicPluginManager pm;
    private final JsonPluginInfos pluginInfos;
    public final AtomicPlugin plugin;

    private final Map<String, Class<?>> classes = new HashMap<>();

    public PluginClassLoader(final Server server, final AtomicPluginManager pluginManager, final ClassLoader parent, final JsonPluginInfos pluginInfos) throws MalformedURLException, InvalidPluginException {
        super(new URL[] { pluginInfos.file.toURI().toURL() }, parent);

        this.server = server;
        this.pm = pluginManager;
        this.pluginInfos = pluginInfos;

        try {
            Class<?> mainPluginClass = this.loadClass(pluginInfos.mainClassName);
            if (AtomicPlugin.class.isAssignableFrom(mainPluginClass)) {
                Constructor<?> constructor = mainPluginClass.getConstructor();
                plugin = (AtomicPlugin) constructor.newInstance();
            }
            else {
                throw new InvalidPluginException("Main class `" + pluginInfos.mainClassName + "` does not extend AtomicPlugin");
            }

        } catch (ClassNotFoundException e) {
            throw new InvalidPluginException("Main class `" + pluginInfos.mainClassName + "` not found", e);
        } catch (InstantiationException e) {
            throw new InvalidPluginException("Abnormal plugin type", e);
        } catch (InvocationTargetException e) {
            throw new InvalidPluginException("InvocationTargetException", e);
        } catch (NoSuchMethodException e) {
            throw new InvalidPluginException("NoSuchMethodException", e);
        } catch (IllegalAccessException e) {
            throw new InvalidPluginException("IllegalAccessException", e);
        }
    }

    @Override
    public Class<?> findClass(String name) throws ClassNotFoundException {
        return findClass(name, true);
    }

    public Class<?> findClass(String name, boolean checkGlobal) throws ClassNotFoundException {
        if (classes.containsKey(name))
            return classes.get(name);
        else {
            Class<?> clazz = null;
            if (checkGlobal) {
                clazz = pm.getClassByName(name);
            }

            if (clazz == null) {
                clazz = super.findClass(name);
                classes.put(name, clazz);
            }

            return clazz;
        }
    }

    public void initialize(AtomicPlugin plugin) {
        if (this.plugin != null) {
            throw new IllegalArgumentException("Plugin `"+plugin.getName()+"` already initialized !");
        }
        plugin.init(server, pluginInfos);
    }
}

package io.atomicserver.server;

import io.atomicserver.api.interfaces.CommandExecutor;
import io.atomicserver.commands.HelpCommand;
import io.atomicserver.plugins.AtomicPluginManager;
import io.atomicserver.sockets.AtomicServer;
import org.jline.reader.*;
import org.jline.terminal.Terminal;
import org.jline.terminal.TerminalBuilder;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class ServerApplication implements Completer {
    private final Terminal terminal;
    private final AtomicServer server;
    private final AtomicPluginManager pm;

    public ServerApplication() throws IOException {
        terminal = TerminalBuilder.terminal();

        server = new AtomicServer();

        pm = (AtomicPluginManager)server.getPluginManager();
        pm.registerCommand("help", new HelpCommand(pm));

        server.open();

        LineReader lineReader = LineReaderBuilder.builder()
                .terminal(terminal)
                .completer(this)
                .build();

        System.out.println("Type a command or stop to quit...");
        String line = null;
        do {
            line = lineReader.readLine();
            if (!line.startsWith("stop")) {
                String[] split = line.split(" ");
                String[] cmdArgs = Arrays.copyOfRange(split, 1, split.length);
                server.getPluginManager().dispatchCommand(split[0], cmdArgs);
            }
        } while (!line.startsWith("stop"));

        server.close();
    }

    @Override
    public void complete(LineReader reader, ParsedLine parsedLine, List<Candidate> list) {
        if (parsedLine.wordIndex() == 0) {
            for(String cmd : pm.getAvailableCommandsNames()) {
                list.add(new Candidate(cmd));
            }
            list.add(new Candidate("stop"));
        }
        else {
            List<CommandExecutor> ls = pm.getCommandExecutors(parsedLine.words().get(0));
            for (CommandExecutor cmd : ls) {
                cmd.onComplete(reader, parsedLine, list);
            }
        }
    }
}

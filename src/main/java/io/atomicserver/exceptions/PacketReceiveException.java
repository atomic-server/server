package io.atomicserver.exceptions;

public class PacketReceiveException extends AtomicServerException {

    public PacketReceiveException(String message, Throwable cause) {
        super(message, cause);
    }
}

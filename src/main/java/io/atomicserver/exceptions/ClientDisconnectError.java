package io.atomicserver.exceptions;

import java.io.IOException;
import java.net.Socket;

public class ClientDisconnectError extends Error {

    public ClientDisconnectError(Socket socket, Throwable cause) {
        super(cause == null ? null : cause.getMessage(), cause);
        if (socket != null && !socket.isClosed()) {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public ClientDisconnectError(Socket socket) {
        this(socket, null);
    }
}

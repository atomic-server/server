package io.atomicserver.exceptions;

public class AtomicServerException extends Exception {
    public AtomicServerException() {
        super();
    }

    public AtomicServerException(String message) {
        super(message);
    }

    public AtomicServerException(String message, Throwable cause) {
        super(message, cause);
    }
}

package io.atomicserver.exceptions;

public class InvalidPluginException extends AtomicServerException {

    public InvalidPluginException(String message) {
        super(message);
    }

    public InvalidPluginException(String message, Throwable cause) {
        super(message, cause);
    }
}

package io.atomicserver.commands;

import io.atomicserver.Utils.AinsiColor;
import io.atomicserver.api.interfaces.CommandExecutor;
import io.atomicserver.api.interfaces.Plugin;
import io.atomicserver.api.interfaces.Server;
import io.atomicserver.plugins.AtomicPluginManager;
import org.jline.reader.Candidate;
import org.jline.reader.LineReader;
import org.jline.reader.ParsedLine;

import java.util.List;

public class HelpCommand implements CommandExecutor {

    private final AtomicPluginManager pm;

    public HelpCommand(AtomicPluginManager pluginManager) {
        this.pm = pluginManager;
    }

    @Override
    public void onCommand(Server server, String name, String[] args) {
        if (args.length != 1) {
            usage(name);
            return ;
        }

        Plugin pl = pm.getPlugin(args[0]);
        if (pl != null) {
            pl.help();
        }
        else {
            List<CommandExecutor> cmds = pm.getCommandExecutors(args[0]);
            if (cmds.size() > 0) {
                for (CommandExecutor cmd : cmds) {
                    Plugin owner = pm.getPluginForObject(cmd);
                    if (owner != null) System.out.println(AinsiColor.yellow + "Command from plugin " + owner.getName() + " :" + AinsiColor.clear);
                    else System.out.println(AinsiColor.yellow + "Server command :" + AinsiColor.clear);

                    cmd.usage(args[0]);
                }
            }
            else {
                System.out.println(AinsiColor.red + "There is no plugin or command named " + args[0]);
            }
        }
    }

    @Override
    public void onComplete(LineReader reader, ParsedLine parsedLine, List<Candidate> list) {
        if (parsedLine.words().size() <= 2) {
            for (Plugin plugin : pm.getPlugins()) {
                list.add(new Candidate(plugin.getName(), plugin.getName(), "Plugins", null, null, null, true));
            }
            for (String cmd : pm.getAvailableCommandsNames()) {
                list.add(new Candidate(cmd, cmd, "Commands", null, null, null, true));
            }
        }
    }

    @Override
    public void usage(String name) {
        System.out.println("Usage: help <Plugin or Command>");
    }
}

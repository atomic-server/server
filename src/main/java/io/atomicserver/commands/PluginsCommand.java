package io.atomicserver.commands;

import io.atomicserver.Utils.AinsiColor;
import io.atomicserver.api.interfaces.CommandExecutor;
import io.atomicserver.api.interfaces.Plugin;
import io.atomicserver.api.interfaces.PluginManager;
import io.atomicserver.api.interfaces.Server;
import org.jline.reader.Candidate;
import org.jline.reader.LineReader;
import org.jline.reader.ParsedLine;

import java.util.List;

public class PluginsCommand implements CommandExecutor {

    private PluginManager pm;

    public PluginsCommand(PluginManager pluginManager) {
        pm = pluginManager;
    }

    @Override
    public void onCommand(Server server, String name, String[] args) {
        int i = 0;
        Plugin[] plugins = pm.getPlugins();
        if (plugins.length > 0) {
            for (Plugin plugin : plugins) {
                System.out.print(AinsiColor.clear);
                if (i++ > 0)
                    System.out.print(", ");
                System.out.print(AinsiColor.green + plugin.getName());
            }
            System.out.println(AinsiColor.clear);
        }
        else
            System.out.println(AinsiColor.red + "There is no plugin" + AinsiColor.clear);
    }

    @Override
    public void onComplete(LineReader reader, ParsedLine parsedLine, List<Candidate> list) {

    }

    @Override
    public void usage(String name) {
        System.out.println("Show currently loaded plugins");
    }
}

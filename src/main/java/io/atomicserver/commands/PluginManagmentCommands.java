package io.atomicserver.commands;

import io.atomicserver.api.interfaces.CommandExecutor;
import io.atomicserver.api.interfaces.Plugin;
import io.atomicserver.api.interfaces.Server;
import io.atomicserver.plugins.JsonPluginInfos;
import io.atomicserver.plugins.AtomicPluginManager;
import org.jline.reader.Candidate;
import org.jline.reader.LineReader;
import org.jline.reader.ParsedLine;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PluginManagmentCommands implements CommandExecutor {
    private final AtomicPluginManager pm;

    public PluginManagmentCommands(AtomicPluginManager pluginManager) {
        this.pm = pluginManager;
    }

    @Override
    public void onCommand(Server server, String name, String[] args) {
        if (name.equals("load")) {
            if (args.length <= 0) {
                usage(name);
                return;
            }

            JsonPluginInfos[] infos = pm.getValidPluginsInfosFromDirectory(true);
            List<Object> toLoad = new ArrayList<>(Arrays.asList((Object[])args));

            for (JsonPluginInfos info : infos) {
                if (toLoad.contains(info.name)) {
                    if (pm.getPlugin(info.name) == null)
                        pm.loadPlugin(info);
                    else
                        System.out.println("Plugin `" + info.name + "` already loaded");
                    toLoad.remove(info.name);
                }
            }

            for (Object unloaded : toLoad) {
                System.err.println("Can't found plugin named `" + unloaded + "`");
            }
        }
        else if (name.equals("unload")) {
            if (args.length <= 0) {
                usage(name);
                return;
            }
            for (String pluginName : args) {
                Plugin plugin = pm.getPlugin(pluginName);
                if (plugin != null) {
                    pm.unloadPlugin(plugin);
                }
                else {
                    System.out.println("Plugin `" + pluginName + "` doesn't exist");
                }
            }
        }
        else if (name.equals("reload")) {
            if (args.length <= 0) {
                pm.unloadPlugins();
                pm.loadPlugins();
            }
            else {
                for (String pluginName : args) {
                    Plugin plugin = pm.getPlugin(pluginName);
                    if (plugin != null) {
                        pm.reloadPlugin(plugin);
                    } else {
                        System.out.println("Plugin `" + pluginName + "` doesn't exist");
                    }
                }
            }
        }
    }

    @Override
    public void onComplete(LineReader reader, ParsedLine parsedLine, List<Candidate> list) {
        String name = parsedLine.words().get(0);

        if (name.equals("unload") || name.equals("reload")) {
            for (Plugin plugin : pm.getPlugins()) {
                if (!parsedLine.words().contains(plugin.getName()))
                    list.add(new Candidate(plugin.getName()));
            }
        }
        else if (name.equals("load")) {
            JsonPluginInfos[] infos = pm.getValidPluginsInfosFromDirectory(true);
            for (JsonPluginInfos info : infos) {
                if (pm.getPlugin(info.name) == null && !parsedLine.words().contains(info.name)) {
                    list.add(new Candidate(info.name));
                }
            }
        }
    }

    @Override
    public void usage(String name) {
        if (name.equals("unload")) {
            System.out.println("Unload specified plugins and plugins which depend on them");
            System.out.println("Usage: unload <plugin name 1> ... [plugin N]");
        }
        else if (name.equals("load")) {
            System.out.println("Load specified plugins and their dependencies");
            System.out.println("Usage: load <plugin name 1> ... [plugin N]");
        }
        else if (name.equals("reload")) {
            System.out.println("Reload specified plugins");
            System.out.println("Usage: reload [plugin name 1] ... [plugin N]");
        }
    }
}

package io.atomicserver.client;

import io.atomicserver.api.events.*;
import io.atomicserver.api.interfaces.ClientApplication;
import io.atomicserver.api.interfaces.Listener;
import io.atomicserver.api.interfaces.TriggerExecutor;
import io.atomicserver.exceptions.AtomicServerException;
import io.atomicserver.exceptions.ClientDisconnectError;
import io.atomicserver.sockets.AtomicClient;
import io.atomicserver.transport.Packet;
import io.atomicserver.transport.RequestPacket;
import io.atomicserver.transport.ResponsePacket;
import io.atomicserver.transport.TriggerPacket;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonValue;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

public class AtomicClientApplication extends AtomicClient implements ClientApplication {

    private Map<String, TriggerExecutor> triggers = new HashMap<>();
    private Thread listenThread = null;
    private List<Listener> listeners = new ArrayList<>();
    private Map<UUID, ResponsePacket.Callback> waitingRequest = new HashMap<>();

    public AtomicClientApplication(String ip, int port) throws IOException {
        super(ip, port);
    }

    @Override
    public void sendRequest(String name, JsonValue args, ResponsePacket.Callback callback) {
        RequestPacket packet = new RequestPacket(name, args);
        if (callback != null)
            waitingRequest.put(packet.getUUID(), callback);
        sendPacket(packet);
    }

    @Override
    public void subscribeToTrigger(String name, TriggerExecutor trigger) {
        this.sendRequest("SUBSCRIBE_TO_TRIGGERS", Json.createArrayBuilder().add(name).build(), (response) -> {
            // TODO : Manage success false (re-send ??)
        });
        triggers.put(name, trigger);
    }

    @Override
    public void registerListener(Listener listener) {
        if (!listeners.contains(listener))
            listeners.add(listener);
    }

    @Override
    public void startListening() {
        if (listenThread == null) {
            listenThread = new Thread(this::listen);
            listenThread.start();
        }
        else {
            System.err.println("WARNING : you call AtomicClient.startListening() on already listening client");
        }
    }

    private void listen() {
        while (!getSocket().isClosed()) {
            try {
                Packet packet = receivePacket();
                //System.out.println("[RECEIVE] type = " + packet.getType());
                //System.out.println("[RECEIVE] " + getUUID().toString() + " <- " + packet.toString());

                if (packet instanceof TriggerPacket) {
                    JsonObject json = (JsonObject) ((TriggerPacket)packet).getJsonValue();
                    String trigger = json.getString("trigger", null);
                    JsonValue args = json.get("args");

                    if (trigger != null) dispatchTrigger(trigger, args);
                    else System.err.println("[WARNING] Request received without name");
                }
                else if (packet instanceof ResponsePacket) {
                    ResponsePacket response = (ResponsePacket) packet;
                    UUID uuid = response.getUUID();

                    if (waitingRequest.containsKey(uuid)) {
                        final ResponsePacket.Callback callback = waitingRequest.get(uuid);
                        new Thread(() -> callback.onResponse(response)).start();
                        waitingRequest.remove(uuid);
                    }
                }
                else {
                    dispatchEvent(new PacketReceivedEvent(this, packet));
                }

            } catch (ClientDisconnectError e) {
                dispatchEvent(new ServerDisconnectedEvent(this));
                break;
            } catch (AtomicServerException e) {
                e.printStackTrace();
            }
        }
    }

    private void dispatchTrigger(String name, JsonValue args) {
        if (triggers.containsKey(name)) {
            final TriggerExecutor trigger = triggers.get(name);
            new Thread(() -> trigger.onTrigger(this, name, args)).start();
        }
        else {
            System.err.println("No trigger found for name " + name);
        }
    }

    private void dispatchEvent(Event event) {
        for (Listener listener : listeners) {
            Method[] methods = listener.getClass().getDeclaredMethods();
            for (Method method : methods) {

                if (method.isAnnotationPresent(EventHandler.class) && method.getParameterCount() == 1 && method.getParameterTypes()[0].equals(event.getClass())) {
                    // EventHandler present, only one parameter and same type as this event
                    new Thread(() -> {
                        try {
                            method.invoke(listener, event);

                        } catch (IllegalAccessException | InvocationTargetException e) {
                            e.printStackTrace();
                        }
                    }).start();
                }
            }
        }
    }
}

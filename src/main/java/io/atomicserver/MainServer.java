package io.atomicserver;

import io.atomicserver.server.ServerApplication;
import java.io.IOException;

public class MainServer {

    public static void main(String[] args) {
        try {
            ServerApplication app = new ServerApplication();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

package io.atomicserver.transport;

import io.atomicserver.Utils.ArgsChecker;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;
import java.util.UUID;

public class ResponsePacket extends JsonPacket {

    public interface Callback {
        void onResponse(ResponsePacket response);
    }

    public ResponsePacket(byte[] bytes) {
        super(bytes);
    }

    private static JsonObject init(String name, UUID uuid, boolean success, String message, JsonValue data) {
        ArgsChecker.check(name != null, "Request response name must not be null");
        ArgsChecker.check(uuid != null, "Request response UUID must not be null");

        JsonObjectBuilder builder = Json.createObjectBuilder()
                .add("request", name)
                .add("UUID", uuid.toString())
                .add("success", success)
                .add("data", data == null ? JsonValue.NULL : data);

        if (message != null)
            builder.add("message", message);

        return builder.build();
    }

    /**
     * Create a RequestResponsePacket
     *
     * @param name The name of the request
     * @param uuid The UUID of the request
     * @param success The request response
     * @param message A response message (can be null)
     * @param data Additional response data
     */
    public ResponsePacket(String name, UUID uuid, boolean success, String message, JsonValue data) {
        this(init(name, uuid, success, message, data).toString().getBytes());
    }

    /**
     * Create a RequestResponsePacket from a RequestPacket
     *
     * @param request The request packet
     * @param success The request response
     * @param data Additional response data
     */
    public ResponsePacket(RequestPacket request, boolean success, String message, JsonValue data) {
        this(request.getName(), request.getUUID(), success, message, data);
    }

    /**
     * Get the request name
     *
     * @return The request name
     */
    public String getName() {
        return getJsonValue().asJsonObject().getString("request");
    }

    /**
     * Get the UUID of the request
     *
     * @return The request UUID
     */
    public UUID getUUID() {
        return UUID.fromString(getJsonValue().asJsonObject().getString("UUID"));
    }

    /**
     * Get the request success
     *
     * @return true if the request success else false
     */
    public boolean isSuccess() {
        return getJsonValue().asJsonObject().getBoolean("success", false);
    }

    /**
     * Get the response message
     *
     * @return The request message (can be null)
     */
    public String getMessage() {
        return getJsonValue().asJsonObject().getString("message", null);
    }

    /**
     * Get the response additional data
     *
     * @return The response data
     */
    public JsonValue getData() {
        return getJsonValue().asJsonObject().getOrDefault("data", null);
    }
}

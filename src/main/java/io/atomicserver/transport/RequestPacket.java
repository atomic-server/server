package io.atomicserver.transport;

import io.atomicserver.Utils.ArgsChecker;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonValue;
import java.util.UUID;

public class RequestPacket extends JsonPacket {

    public RequestPacket(byte[] bytes) {
        super(bytes);
    }

    private static JsonObject init(String name, JsonValue args) {
        ArgsChecker.check(name != null, "Request name must not be null");
        return Json.createObjectBuilder()
                .add("request", name)
                .add("UUID", UUID.randomUUID().toString())
                .add("args", args == null ? JsonValue.NULL : args)
                .build();
    }

    /**
     * Create a RequestPacket with given name and arguments
     *
     * @param name The name
     * @param args The arguments
     */
    public RequestPacket(String name, JsonValue args) {
        this(init(name,args).toString().getBytes());
    }

    /**
     * Get the request name
     *
     * @return The request name
     */
    public String getName() {
        return getJsonValue().asJsonObject().getString("request");
    }

    /**
     * Get the UUID of the request
     * <p>
     * UUID is generated when RequestPacket is build
     * </p>
     * @return
     */
    public UUID getUUID() {
        return UUID.fromString(getJsonValue().asJsonObject().getString("UUID"));
    }

    /**
     * Get the request arguments
     *
     * @return The request arguments as JsonValue
     */
    public JsonValue getArgs() {
        return getJsonValue().asJsonObject().getOrDefault("args", null);
    }
}

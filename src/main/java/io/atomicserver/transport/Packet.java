package io.atomicserver.transport;

public abstract class Packet {

    protected final PacketType type;
    protected final byte[] bytes;

    public Packet(byte[] bytes) {
        this.type = PacketType.getByClass(getClass());
        assert type != null : "There is no PacketType defined for class" + getClass().getSimpleName();

        this.bytes = bytes;
    }

    public PacketType getType() {
        return type;
    }

    public byte[] getBytes() {
        return bytes;
    }

    @Override
    public String toString() {
        return new String(bytes);
    }
}

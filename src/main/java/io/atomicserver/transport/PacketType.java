package io.atomicserver.transport;

public enum PacketType {
    // !!! IMPORTANT !!! DON'T USE SAME ID OR CLASS FOR DIFFERENT PacketType
    RAW(0, RawPacket.class),
    REQUEST(10, RequestPacket.class),
    REQUEST_RESPONSE(11, ResponsePacket.class),
    TRIGGER(20, TriggerPacket.class),
    JSON(30, JsonPacket.class);

    private int id;
    private Class<? extends Packet> clazz;

    PacketType(int id, Class<? extends Packet> clazz) {
        this.id = id;
        this.clazz = clazz;
    }

    public int getId() {
        return id;
    }

    public Class<? extends Packet> getPacketClass() {
        return clazz;
    }

    public static PacketType getById(int id) {
        for (PacketType value : PacketType.values()) {
            if (value.id == id)
                return value;
        }
        return null;
    }

    public static PacketType getByClass(Class<? extends Packet> clazz) {
        for (PacketType value : PacketType.values()) {
            if (value.clazz.equals(clazz))
                return value;
        }
        return null;
    }
}

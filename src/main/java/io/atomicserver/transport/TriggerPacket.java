package io.atomicserver.transport;

import io.atomicserver.Utils.ArgsChecker;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonValue;

public class TriggerPacket extends JsonPacket {

    public TriggerPacket(byte[] bytes) {
        super(bytes);
    }

    private static JsonObject init(String name, JsonValue args) {
        ArgsChecker.check(name != null, "Trigger name must not be null");
        return Json.createObjectBuilder()
                .add("trigger", name)
                .add("args", args == null ? JsonValue.NULL : args)
                .build();
    }

    /**
     * Create a TriggerPacket with given name and arguments
     *
     * @param name The name
     * @param args The arguments
     */
    public TriggerPacket(String name, JsonValue args) {
        this(init(name, args).toString().getBytes());
    }

    /**
     * Get the trigger name
     *
     * @return The trigger name
     */
    public String getName() {
        return getJsonValue().asJsonObject().getString("trigger");
    }

    /**
     * Get the trigger arguments
     *
     * @return The trigger arguments as JsonValue
     */
    public JsonValue getArgs() {
        return getJsonValue().asJsonObject().getOrDefault("args", null);
    }
}

package io.atomicserver.transport;

public class RawPacket extends Packet {

    public RawPacket(byte[] bytes) {
        super(bytes);
    }
}

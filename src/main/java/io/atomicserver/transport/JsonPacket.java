package io.atomicserver.transport;

import io.atomicserver.Utils.JsonTools;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonValue;
import java.io.StringReader;

public class JsonPacket extends Packet {

    private JsonValue jsonValue = null;

    public JsonPacket(byte[] bytes) {
        super(bytes);
    }
    public JsonPacket(JsonValue json) {
        this(json.toString().getBytes());
    }

    @Override
    public String toString() {
        JsonValue json = getJsonValue();
        if (json == null)
            return "{null}";
        else
            return json.toString();
    }

    public JsonValue getJsonValue() {
        if (jsonValue == null) {
            String jsonString = new String(bytes);
            jsonValue = Json.createReader(new StringReader(jsonString)).readObject();
        }
        return jsonValue;
    }
}

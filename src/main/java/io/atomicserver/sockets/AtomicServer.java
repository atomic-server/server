package io.atomicserver.sockets;

import io.atomicserver.Utils.JsonTools;
import io.atomicserver.api.events.ClientConnectedEvent;
import io.atomicserver.api.events.ClientDisconnectedEvent;
import io.atomicserver.api.events.PacketReceivedEvent;
import io.atomicserver.api.interfaces.Client;
import io.atomicserver.api.interfaces.PluginManager;
import io.atomicserver.exceptions.AtomicServerException;
import io.atomicserver.exceptions.ClientDisconnectError;
import io.atomicserver.plugins.AtomicPluginManager;
import io.atomicserver.requests.SubscribeToTriggersRequest;
import io.atomicserver.transport.Packet;
import io.atomicserver.transport.RequestPacket;
import io.atomicserver.transport.TriggerPacket;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonValue;
import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;

/**
 * Represent a ServerSocket that manage the AtomicServer
 */
public class AtomicServer extends LoggerServer {

    private ServerSocket serverSocket;

    private Thread listenThread;

    private List<Client> clients;
    private AtomicPluginManager pluginManager;

    private SubscribeToTriggersRequest subscribeToTriggers;

    private JsonObject serverConfig = null;

    /**
     * Open the AtomicServer with host, port and backlog specified in config.json
     *
     * @exception  IOException  if an I/O error occurs when opening the socket.
     * @exception  SecurityException if a security manager exists and its {@code checkListen}
     *             method doesn't allow the operation.
     * @exception  IllegalArgumentException if the port parameter is outside
     *             the specified range of valid port values, which is between
     *             0 and 65535, inclusive.
     */
    public AtomicServer() throws IOException {

        clients = new LinkedList<>();

        String host = getConfig().getString("host", "any");
        int port = getConfig().getInt("port", 36666);
        int backlog = getConfig().getInt("backlog", 100);

        info("Loading Atomic Server on address " + host + " and port " + port);

        if (host.equalsIgnoreCase("any"))
            host = null;

        if (host != null) serverSocket = new ServerSocket(port, backlog, InetAddress.getByName(host));
        else serverSocket = new ServerSocket(port, backlog);

        pluginManager = new AtomicPluginManager(this);

        subscribeToTriggers = new SubscribeToTriggersRequest();
        pluginManager.registerRequest("SUBSCRIBE_TO_TRIGGERS", subscribeToTriggers);

        pluginManager.loadPlugins();
    }

    /**
     * Start a new thread to wait for client connection.
     * Show a warning message if server is already opened
     */
    public void open() {
        if (listenThread == null) {
            listenThread = new Thread(this::listenConnection);
            listenThread.start();
        }
        else {
            System.err.println("WARNING : you call AtomicServer.open() on already opened server");
        }
    }

    /**
     * Ask for closing the "waiting connection" thread
     * No effect if the thread is already closed
     */
    public void close() {
        try {
            pluginManager.unloadPlugins();
            for (Client client : clients)
                ((AtomicClient)client).close();
            serverSocket.close();
            listenThread = null;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Waiting connection loop for listenThread.
     * Create client instance on connection.
     */
    private void listenConnection() {
        while (!serverSocket.isClosed()) {
            try {
                Socket socket = serverSocket.accept();
                info("New client connected : " + socket.getInetAddress().toString());

                final AtomicClient client = new AtomicClient(socket);
                clients.add(client);
                new Thread(() -> { listenClient(client); }).start();

                try {
                    pluginManager.dispatchEvent(new ClientConnectedEvent(client));
                } catch (ClientDisconnectError e) {
                    disconnectClient(client);
                }
            } catch (IOException e) {
                if (listenThread != null) // Prevent showing exception when socket closed
                    e.printStackTrace();
            }
        }

        try {
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        listenThread = null;
    }

    /**
     * Waiting read loop for a client thread.
     * Remove client instance on disconnection.
     *
     * @param client The client for which the loop is
     */
    private void listenClient(AtomicClient client) {

        while (!client.getSocket().isClosed()) {
            try {
                Packet packet = client.receivePacket();
                System.out.println("[RECEIVE] " + packet.toString());

                if (packet instanceof RequestPacket) {
                    pluginManager.dispatchRequest(client, (RequestPacket)packet);
                }
                else {
                    pluginManager.dispatchEvent(new PacketReceivedEvent(client, packet));
                }

            } catch (ClientDisconnectError e) {
                disconnectClient(client);
                break;
            } catch (AtomicServerException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void dispatchTrigger(String name, JsonValue args) {

        List<Client> targets = subscribeToTriggers.getSubscribers(name);
        if (targets.size() > 0) {
            Packet trigger = new TriggerPacket(name, args);

            for (int i = 0; i < targets.size(); i++) {
                Client target = targets.get(i);
                try {
                    target.sendPacket(trigger);

                } catch (ClientDisconnectError e) {
                    targets.remove(i);
                    disconnectClient(target);
                    i--;
                }
            }
        }
    }

    private void disconnectClient(Client client) {
        if (clients.contains(client)) {
            info("Client disconnected " + client.getInetAddress().toString());
            clients.remove(client);
            try {
                pluginManager.dispatchEvent(new ClientDisconnectedEvent(client));
            } catch (ClientDisconnectError e) {
                System.err.println("Check at AtomicServer.java:disconnectClient, why printStackTrace ???");
                e.printStackTrace();
            }
        }
    }

    @Override
    public List<Client> getClients() {
        return clients;
    }

    @Override
    public void broadcastPacket(Packet packet) {
        for(Client client : clients) {
            client.sendPacket(packet);
        }
    }

    @Override
    public PluginManager getPluginManager() {
        return pluginManager;
    }

    @Override
    public JsonObject getConfig() {
        if (serverConfig == null)
            return reloadConfig();
        else
            return serverConfig;
    }

    @Override
    public JsonObject reloadConfig() {
        InputStream in = null;
        File configFile = new File("config.json");

        // If file exists in directory, we use it
        try {
            if (configFile.exists())
                in = new FileInputStream(configFile);
        } catch (FileNotFoundException e) {
            log(Level.WARNING, "Error reading config.json", e);
        }

        // Else, we try to use the file from jar, and write it in dir
        if (in == null) {
            in = getClass().getResourceAsStream("/config.json");

            try {
                byte[] bytes = in.readAllBytes();
                configFile.createNewFile();
                OutputStream out = new FileOutputStream(configFile);
                out.write(bytes);
                out.flush();
                serverConfig = Json.createReader(new StringReader(new String(bytes))).readObject();
            } catch (IOException e) {
                log(Level.WARNING, "Error writing config.json", e);
            }
        }
        else
            serverConfig = JsonTools.createReader(in).readObject();

        return serverConfig;
    }
}

package io.atomicserver.sockets;

import io.atomicserver.Utils.AinsiColor;
import io.atomicserver.api.interfaces.Server;

import java.util.Date;
import java.util.logging.*;

public abstract class LoggerServer implements Server {
    private final Logger logger;

    protected LoggerServer() {
        logger = Logger.getLogger("io.atomicserver");
        initLogger();
    }

    @Override
    public Logger getLogger() {
        return logger;
    }

    private void initLogger() {
        logger.setUseParentHandlers(false);
        ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new SimpleFormatter() {
            private static final String format = "%1$tF %1$tT - %2$-4s %3$s %4$s %n";

            @Override
            public synchronized String format(LogRecord lr) {
                return String.format(format,
                        new Date(lr.getMillis()),
                        getColorForLevel(lr.getLevel()) + lr.getLevel().getName().substring(0, 4) + AinsiColor.clear,
                        lr.getMessage(),
                        lr.getThrown() == null ? "" : lr.getThrown().toString()
                );
            }
        });
        logger.addHandler(handler);
    }

    private String getColorForLevel(Level level) {
        if (level == Level.INFO) return AinsiColor.blue;
        else if (level == Level.WARNING) return AinsiColor.yellow;
        else if (level == Level.SEVERE) return AinsiColor.red;
        else return AinsiColor.clear;
    }

    @Override public void info(String msg) { logger.info(msg); }
    @Override public void warning(String msg) { logger.warning(msg); }
    @Override public void severe(String msg) { logger.severe(msg); }
    @Override public void log(Level level, String msg) { logger.log(level, msg); }
    @Override public void log(Level level, String msg, Throwable throwable) { logger.log(level, msg, throwable); }
}

package io.atomicserver.sockets;

import io.atomicserver.Utils.StreamUtils;
import io.atomicserver.api.interfaces.Client;
import io.atomicserver.exceptions.ClientDisconnectError;
import io.atomicserver.exceptions.PacketReceiveException;
import io.atomicserver.transport.Packet;
import io.atomicserver.transport.PacketType;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Constructor;
import java.net.InetAddress;
import java.net.Socket;
import java.util.UUID;

public class AtomicClient implements Client {
    private final Socket socket;
    private final UUID uuid;

    public AtomicClient(Socket client) {
        this.socket = client;
        this.uuid = UUID.randomUUID();
    }

    public AtomicClient(String host, int port) throws IOException {
        this(new Socket(host, port));
    }

    public UUID getUUID() {
        return uuid;
    }

    public void close() {
        try {
            if (!socket.isClosed())
                socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Socket getSocket() {
        return socket;
    }

    @Override
    public Packet receivePacket() throws PacketReceiveException {

        try {
            InputStream stream = socket.getInputStream();
            int typeID = StreamUtils.readInt(stream);
            PacketType packetType = PacketType.getById(typeID);

            if (packetType != null) {
                Class<? extends Packet> clazz = packetType.getPacketClass();
                Constructor<? extends Packet> constructor = clazz.getConstructor(byte[].class);

                int size = StreamUtils.readInt(stream);

                byte[] bytes = new byte[size];
                int len = 0;
                do {
                    len += stream.read(bytes, len, size - len);
                    if (len < 0)
                        throw new ClientDisconnectError(socket);
                } while (len < size);

                return constructor.newInstance((Object) bytes);
            }
            else {
                throw new PacketReceiveException("Unknow packet type number " + typeID, null);
            }

        } catch (IOException e) {
            throw new ClientDisconnectError(socket, e);
        } catch (Exception e) {
            throw new PacketReceiveException("Error during packet reception", e);
        }
    }

    @Override
    public void sendPacket(Packet packet) {
        try {
            OutputStream stream = socket.getOutputStream();

            StreamUtils.writeInt(stream, packet.getType().getId());
            StreamUtils.writeInt(stream, packet.getBytes().length);
            stream.write(packet.getBytes());
            stream.flush();
        } catch (IOException e) {
            throw new ClientDisconnectError(socket);
        }
    }

    @Override
    public InetAddress getInetAddress() {
        return socket.getInetAddress();
    }
}

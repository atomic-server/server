package io.atomicserver.requests;

import io.atomicserver.Utils.ArgsChecker;
import io.atomicserver.api.interfaces.Client;
import io.atomicserver.api.interfaces.RequestExecutor;
import io.atomicserver.api.interfaces.Server;
import io.atomicserver.transport.RequestPacket;
import io.atomicserver.transport.ResponsePacket;

import javax.json.JsonArray;
import javax.json.JsonValue;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class SubscribeToTriggersRequest implements RequestExecutor {

    private Map<String, List<Client>> triggers;

    public SubscribeToTriggersRequest() {
        triggers = new HashMap<>();
    }

    public List<Client> getSubscribers(String trigger) {
        return triggers.getOrDefault(trigger, new LinkedList<>());
    }

    @Override
    public ResponsePacket onRequest(Server server, Client sender, RequestPacket packet) {
        ArgsChecker.check(packet.getArgs().getValueType() == JsonValue.ValueType.ARRAY, "SUBSCRIBE_TRIGGERS received without array");

        boolean success = true;
        String message = null;

        JsonArray array = packet.getArgs().asJsonArray();
        for (int i = 0; i < array.size(); i++) {
            String triggerName = array.getString(i, null);
            if (triggerName != null) {
                List<Client> ls = triggers.getOrDefault(triggerName, new LinkedList<Client>());
                ls.add(sender);
                triggers.put(triggerName, ls);
            }
            else {
                System.err.println("SUBSCRIBE_TO_TRIGGERS without string name");
                success = false;
                message = "SUBSCRIBE_TO_TRIGGERS without string name";
            }
        }
        return new ResponsePacket(packet, success, message, null);
    }
}

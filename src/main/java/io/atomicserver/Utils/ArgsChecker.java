package io.atomicserver.Utils;

public abstract class ArgsChecker {

    public static void check(boolean valid, String msg) {
        if (!valid)
            throw new IllegalArgumentException(msg);
    }
}

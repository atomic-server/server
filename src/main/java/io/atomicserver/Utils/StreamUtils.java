package io.atomicserver.Utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public abstract class StreamUtils {

    public static void writeInt(OutputStream stream, int value) throws IOException {
        byte[] buffer = new byte[4];
        for (int i = 3; i >= 0; i--) {
            buffer[3 - i] = (byte) ((value >> (8 * i)) & 0xFF);
        }
        stream.write(buffer);
    }

    public static int readInt(InputStream stream) throws IOException {
        byte[] buffer = new byte[4];
        int value = 0;

        int len = stream.read(buffer);
        if (len < 4)
            throw new IOException("Attempt to read int with only " + len + " bytes");

        for (int i = 0; i < 4; i++) {
            value = value << 8;
            value += Byte.toUnsignedInt(buffer[i]);
        }
        return value;
    }
}

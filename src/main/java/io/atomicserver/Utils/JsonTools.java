package io.atomicserver.Utils;

import javax.json.*;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public abstract class JsonTools {

    public static JsonReader createReader(InputStream in) {
        return Json.createReaderFactory(null).createReader(in, StandardCharsets.UTF_8);
    }

    public static String[] getArray(JsonObject json, String key, String[] fallback) {
        JsonArray array = json.getJsonArray(key);
        if (array != null) {
            String[] result = new String[array.size()];
            for (int i = 0; i < array.size(); i++)
                result[i] = array.getString(i);
            return result;
        }
        else
            return fallback;
    }

    public static JsonArray toJsonArray(String... array) {
        JsonArrayBuilder builder = Json.createArrayBuilder();
        for (String el : array)
            builder.add(el);
        return builder.build();
    }
}

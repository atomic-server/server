# What is AtomicServer ?
AtomicServer is several things at the same time, the whole allowing the implementation of a client-server architecture, in a simplified way and customizable as you wish. First of all, a software is used as a base for the server and will manage all communications with the client. Secondly, a Java library for the client allows to manage the communications with the server. Finally, a library for the server, also in Java, gives you the possibility to develop your specific functionalities within a plugin. Plugins can depend on each other allowing maximum modularity and thus avoid duplicating code.

# How it's work ?
Various concepts have been implemented:

- **Commands**: allows you to execute a custom command in the server console.
- **Requests**: similar to routes in WebServices, allows the client to request something from the server.
- **Triggers**: allows the server to notify a client of a particular event.
- **Listeners**: allows the server to intercept different events such as the connection of a new client or the reception of a custom packet.

# Project progress
As you can see, this is quite a large project. The basic functional bricks are there and you can already experiment with it. However, there is a lot to do before it becomes a production project:

- Encryption of communications (everything is clear for now).
- Development of the client library for the main platforms (Node, PHP, Android & iOS).
- Improve plugin security (containerized plugins in their respective folders).
- Implementation of unit tests / properties tests.
- Improvement of the Wiki and the doc.
- Add features according to different user needs.

The project is of course open source (X11 License) and all contributions are welcome.

Website & Forum : https://www.atomicserver.io/  
Wiki : https://www.atomicserver.io/wiki  
Documentation : https://docs.atomicserver.io/  